package com.kittaphat.midterm;

public class Obstruction extends Unit{
    public Obstruction(Map map, int x, int y) {
        super(map, 'O', x, y, true);
        
    }
}
