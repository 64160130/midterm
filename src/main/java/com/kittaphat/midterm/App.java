package com.kittaphat.midterm;

import java.util.Scanner;

public class App {
    static Map map = new Map(25, 25);
    static Runner runner = new Runner(map, 'R', 12, 24);
    static Obstruction obstruction1 = new Obstruction(map, 5, 5);
    static Obstruction obstruction2 = new Obstruction(map, 5, 6);
    static Obstruction obstruction3 = new Obstruction(map, 6, 5);
    static Obstruction obstruction4 = new Obstruction(map, 7, 5);
    static Obstruction obstruction5 = new Obstruction(map, 5, 7);
    static Obstruction obstruction6 = new Obstruction(map, 6, 7);
    static Obstruction obstruction7 = new Obstruction(map, 11, 9);
    static Obstruction obstruction8 = new Obstruction(map, 11, 10);
    static Obstruction obstruction9 = new Obstruction(map, 11, 12);
    static Obstruction obstruction10 = new Obstruction(map, 12, 10);
    static Obstruction obstruction11 = new Obstruction(map, 20, 10);
    static Obstruction obstruction12 = new Obstruction(map, 12, 20);
    static Obstruction obstruction13 = new Obstruction(map, 19, 22);
    static Obstruction obstruction14 = new Obstruction(map, 18, 5);
    static Obstruction obstruction15 = new Obstruction(map, 19, 7);
    static Obstruction obstruction16 = new Obstruction(map, 16, 8);
    static Obstruction obstruction17 = new Obstruction(map, 12, 4);
    static Obstruction obstruction18 = new Obstruction(map, 10, 15);
    static Obstruction obstruction19 = new Obstruction(map, 23, 6);
    static Obstruction obstruction20 = new Obstruction(map, 20, 7);
    static Obstruction obstruction21 = new Obstruction(map, 16, 9);
    static Obstruction obstruction22 = new Obstruction(map, 15, 3);
    static Obstruction obstruction23 = new Obstruction(map, 13, 13);
    static Obstruction obstruction24 = new Obstruction(map, 19, 19);
    static Obstruction obstruction25 = new Obstruction(map, 17, 17);
    static Obstruction obstruction26 = new Obstruction(map, 16, 17);
    static Obstruction obstruction27 = new Obstruction(map, 2, 17);
    static Obstruction obstruction28 = new Obstruction(map, 3, 15);
    static Obstruction obstruction29 = new Obstruction(map, 18, 17);
    static Obstruction obstruction30 = new Obstruction(map, 11, 5);
    static Scanner sc = new Scanner(System.in);

    public static String input() {
        return sc.next();
    }

    public static void process(String command) {
        switch (command) {
            case "w":
                runner.up();
                break;
            case "s":
                runner.down();
                break;
            case "a":
                runner.left();
                break;
            case "d":
                runner.right();
                break;
            case "q":
                System.out.println("You give up");
                System.exit(0);
                break;
            case "f":
                System.out.println("You are Winner");
                System.exit(0);
            default:
                break;
        }
    }

    public static void main(String[] args) {
        map.add(obstruction1);
        map.add(obstruction2);
        map.add(obstruction3);
        map.add(obstruction4);
        map.add(obstruction5);
        map.add(obstruction6);
        map.add(obstruction7);
        map.add(obstruction8);
        map.add(obstruction9);
        map.add(obstruction10);
        map.add(obstruction11);
        map.add(obstruction12);
        map.add(obstruction13);
        map.add(obstruction14);
        map.add(obstruction15);
        map.add(obstruction16);
        map.add(obstruction17);
        map.add(obstruction18);
        map.add(obstruction19);
        map.add(obstruction20);
        map.add(obstruction21);
        map.add(obstruction22);
        map.add(obstruction23);
        map.add(obstruction24);
        map.add(obstruction25);
        map.add(obstruction26);
        map.add(obstruction27);
        map.add(obstruction28);
        map.add(obstruction29);
        map.add(obstruction30);
        map.add(runner);

        while (true) {
            System.out.println("Rule");
            System.out.println("1.คุณต้องนำตัวละครนักวิ่ง(Runner)ไปให้ถึงจุดหมาย(บนสุดของMap)และต้องหลบหลีกสิ่งกีดขวางไปด้วย");
            System.out.println("2.คุณจะสามารถควบคุมนักวิ่งได้ด้วย w,a,s,d ");
            System.out.println("3.เมื่อคุณไปถึงบนสุดของMapให้คุณกด f เพื่อสิ้นสุดการวิ่งและจะมีข้อความแสดงว่าคุณชนะ");
            System.out.println("4.แต่หากคุณยังไปไม่ถึงบนสุดแล้วไม่ต้องการวิ่งต่อให้คุณกด q เพื่อสิ้นสุดการวิ่งและจะมีข้อความประกาศว่าคุณยอมแพ้");
            map.print();
            String command = input();
            process(command);
        }
    }
}
